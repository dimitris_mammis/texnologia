package minusxl;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JFileChooser;

import models.BoolCell;
import models.Cell;
import models.Concat;
import models.RealCell;
import models.Remove;
import models.Sheet;
import models.StringCell;
import models.Trim;

public class MinusXL {
	
	public static void main(String[] args) {
		StringCell c = new StringCell(0, 1, "a aabb");
		StringCell c2 = new StringCell(0, 2, "bb");
		
		ArrayList<Cell> l = new ArrayList<Cell>();
		l.add(c2);
		l.add(c);
		
		Concat con = new Concat(2, 2, l);
		con.calculateValue();
		
		Trim tr = new Trim(2, 2, l);
		tr.calculateValue();
		
		Remove rm = new Remove(2, 2, l);
		rm.calculateValue();
		
		System.out.println("concat: "+con.getValue());
		System.out.println("trim: "+tr.getValue());
		System.out.println("remove: "+rm.getValue());
	/*	
		Sheet t = new Sheet("test_tasos", 2,3);
		t.addCell(new BoolCell(0, 0, true),0,0 );
		t.addCell(new RealCell(0, 1, 1.4),0,1 );
		t.addCell(new StringCell(0, 2, "yomeman"),0,2 );
		t.addCell(new RealCell(1, 0, 3),1,0 );
		t.addCell(new RealCell(1, 1, 5.1),1,1 );
		t.addCell(new RealCell(1, 2, 3.1),1,2 );
		
		t.save2CSV();
		*/
		// file chooser
		Component parent = null;
		JFileChooser chooser = new JFileChooser();
	//    FileNameExtensionFilter filter = new FileNameExtensionFilter("csv");
	//    chooser.setFileFilter(filter);
	    
		int returnVal = chooser.showOpenDialog(parent);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	       System.out.println("You chose to open this file: " +
	            chooser.getSelectedFile().getName());
	    }
		Sheet tas = Sheet.load2CSV(chooser.getSelectedFile().getName());
		System.out.println(tas.toString());
	}

}
