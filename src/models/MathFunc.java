package models;

import java.util.ArrayList;

public abstract class MathFunc extends FuncCell{
	
	protected Double value;

	public MathFunc(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
	}

	
	@Override
	public String toString() {
		return ""+value;
	}
	
	public Object getValue(){
		return value;
	}

}
