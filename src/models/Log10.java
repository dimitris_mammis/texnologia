package models;

import java.util.ArrayList;

public class Log10 extends MathFunc {

	public Log10(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		if(cells.size()==1){
			value = Math.log10((Double)(cells.get(0).getValue()));
		}else{
			value = null;
		}
	}
}
