package models;

import java.util.ArrayList;

public class Remove extends StringFunc {

	public Remove(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		String s1 = new String((String) cells.get(0).getValue());
		String s2 = (String) cells.get(1).getValue();
		s1.replace(s2, "");
		value = s1;
	}

}
