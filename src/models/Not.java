package models;

import java.util.ArrayList;

public class Not extends LogicFunc {

	public Not(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		Boolean b1 = (Boolean) cells.get(0).getValue();
		value = !b1;
	}

}
