package models;

import java.util.ArrayList;

public abstract class LogicFunc extends FuncCell {
	
	protected Boolean value;

	public LogicFunc(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return ""+value;
	}
	
	public Object getValue(){
		return value;
	}

}
