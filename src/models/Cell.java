package models;

public class Cell {
	private int x;
	private int y;
	public Cell(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "";
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Object getValue(){
		return 0;
	}

}
