package models;

import java.util.ArrayList;

public class Mult extends MathFunc {

	public Mult(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		double mu = 1;
		for(Cell c:cells){
			mu*=(Double)c.getValue();
		}
		value = mu;
	}

}
