package models;

public class RealCell extends Cell{
	
	private Double value;

	public RealCell(int x, int y, double value) {
		super(x, y);
		this.value = value;
	}
	
	
	@Override
	public String toString() {
		return ""+value;
	}
	
	public Double getValue() {
		return value;
	}
	

}
