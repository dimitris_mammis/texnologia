package models;

import java.util.ArrayList;

public class Concat extends StringFunc {

	public Concat(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		value = (String)cells.get(0).getValue() + (String)cells.get(1).getValue();
	}

}
