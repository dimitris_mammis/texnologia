package models;

import java.util.ArrayList;

public abstract class StringFunc extends FuncCell{

	protected String value;

	public StringFunc(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		
	}


	@Override
	public String toString() {
		return ""+value;
	}
	
	public Object getValue(){
		return value;
	}

}
