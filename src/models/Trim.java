package models;

import java.util.ArrayList;

public class Trim extends StringFunc {
	
	

	public Trim(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		String str = new String((String) cells.get(0).getValue());
		value = str.replace(" ", "");
	}

}
