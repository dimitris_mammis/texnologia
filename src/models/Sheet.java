package models;

import java.awt.Component;
import java.io.*;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Sheet {
	
	private String name;
	private Cell [][] cells;
	private int M;
	private int N;
	
	public Sheet() {
		// TODO Auto-generated constructor stub
	}
	
	public Sheet(String name, int m, int n) {
		this.name = name;
		cells = new Cell[m][n];
		M=m;
		N=n;
	}

	public static Sheet load2CSV(String name){
		int[] rowcol = new int[2];
		rowcol = count_lines(name);
		String[] sheet_name = name.split(".csv");
		Sheet tmp = new Sheet(sheet_name[0], rowcol[0], rowcol[1]);
		BufferedReader reader = null;
		String dataline;
		String[] data;
		Cell[][] tmpCell = new Cell[rowcol[0]][rowcol[1]];
		int line = 0;
		
		try{
			reader = new BufferedReader(new FileReader(name));
			while ((dataline = reader.readLine()) != null){
				data = dataline.split(";");
				for(int i=0;i<data.length;i++){
					try{
						double value = Double.parseDouble(data[i]);
						tmpCell[line][i] = new RealCell(line, i, value);
					}catch(Exception e){
						if(data[i].equals("true")){
							tmpCell[line][i] = new BoolCell(line, i, true);
						}else if(data[i].equals("false")){
							tmpCell[line][i] = new BoolCell(line, i, false);
						}else{
							tmpCell[line][i] = new StringCell(line, i, data[i]);
						}
					}
				}
				line++;
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try{
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		tmp.setCell(tmpCell);
	    return tmp;
	}
	
	private void setCell(Cell[][] tmpCell) {
		this.cells = tmpCell;
	}

	private static int[] count_lines(String name){
		BufferedReader reader = null;
		String dataline;
		int lines = 0;
		int columns = 0;
		int[] rowcol = new int[2];
		int maxcolumns = 0;
		try{
			reader = new BufferedReader(new FileReader(name));
			while ((dataline = reader.readLine()) != null){
				columns = (int) dataline.split(";").length;
				if(columns > maxcolumns)
					maxcolumns = columns;
				lines++;
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("lines "+lines + " Columns "+maxcolumns);
			rowcol[0] = lines;
			rowcol[1] = maxcolumns;
		}
		return rowcol;
	}
	
	public int getM() {
		return M;
	}
	
	public int getN() {
		return N;
	}
	
	public Cell[][] getCells() {
		return cells;
	}
	
	public void addCell(Cell c, int m, int n){
		cells[m][n] = c;
	}
	
	public void save2CSV(){
		String file_name = this.name + ".csv";
        BufferedWriter output = null;
        Component dialog = null;
        try{
            File file = new File(file_name);
            output = new BufferedWriter(new FileWriter(file));
        }catch (IOException e){
            e.printStackTrace();
        }finally{
			try{
				//Antigrafi tou sheet sto file
	            for(int i=0;i<M;i++){
	            	for(int j=0;j<N;j++){
	            		output.write(this.cells[i][j].toString());
	            		if(j<N-1)
	            			output.write(";");
	            	}
	            	if(i<M-1)
	            		output.newLine();
	            }  
				output.close();
				JOptionPane.showMessageDialog(dialog,"File "+this.name+" saved!");
			}catch (IOException e){
				e.printStackTrace();
			}
        }
	}

	@Override
	public String toString() {
		String str = new String();
		for(int i=0;i<M;i++){
			for(int j=0;j<N;j++){
				try{
					str += " "+ cells[i][j].toString();
				}catch(Exception e){
					
				}
			}
		}
		return str;
	}

}
