package models;

import java.util.ArrayList;

public class Book {
	
	private String name;
	private ArrayList<Sheet> sheets;
	public Book(String name) {
		super();
		this.name = name;
		sheets = new ArrayList<Sheet>();
	}
	
	
	public void addSheet(Sheet s){
		sheets.add(s);
	}
	
	public void removeSheetByPosition(int index){
		sheets.remove(index);
	}

	
	public ArrayList<Sheet> getSheets() {
		return sheets;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", sheets=" + sheets.toString() + "]";
	}
	
	
	
	

}
