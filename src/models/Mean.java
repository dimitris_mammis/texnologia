package models;
import java.util.ArrayList;

public class Mean extends StatisticFunc {

	public Mean(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void calculateValue() {
		double sum = 0;
		for(Cell c:cells){
			sum+=(Double)c.getValue();
		}
		value = sum/cells.size();
	}

}
