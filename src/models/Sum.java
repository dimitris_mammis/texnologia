package models;

import java.util.ArrayList;

public class Sum extends MathFunc{

	

	public Sum(int x, int y, ArrayList<Cell> cells) {
		super(x, y, cells);
	}

	@Override
	public void calculateValue() {
		double sum = 0;
		for(Cell c:cells){
			sum+=(Double)c.getValue();
		}
		value = sum;
	}
}
