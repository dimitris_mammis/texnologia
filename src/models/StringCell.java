package models;

public class StringCell extends Cell{
	private String value;

	public StringCell(int x, int y, String value) {
		super(x, y);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return value;
	}

}
