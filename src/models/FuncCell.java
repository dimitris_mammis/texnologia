package models;

import java.util.ArrayList;

public abstract class FuncCell extends Cell{
	
	protected ArrayList<Cell> cells;
	
	public FuncCell(int x, int y, ArrayList<Cell> cells) {
		super(x, y);
		this.cells = cells;
	}





	public abstract void calculateValue();

}
