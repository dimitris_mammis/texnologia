package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Book;
import models.Sheet;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class BookWindow extends JFrame {

	private JPanel contentPane;
	
	private DefaultListModel<Sheet> model;
	private JList list;

	
	
	private Book book;
	private JTextField tvName;
	private JTextField tvM;
	private JTextField tvN;
	private JButton btnDelete;
	private JButton btnSaveToCsv;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookWindow frame = new BookWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BookWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 679, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		model = new DefaultListModel<Sheet>();
		
		list = new JList(model);
		
		
		
		tvName = new JTextField();
		tvName.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = tvName.getText().toString();
				String m = tvM.getText().toString();
				String n = tvN.getText().toString();
				
				try{
					int mm = Integer.parseInt(m);
					int nn = Integer.parseInt(n);
					addSheet(new Sheet(name, mm, nn));
				}catch(Exception ee){
					
				}
			}
		});
		
		JLabel lblName = new JLabel("Name");
		
		JLabel lblM = new JLabel("M");
		
		tvM = new JTextField();
		tvM.setColumns(10);
		
		JLabel lblN = new JLabel("N");
		
		tvN = new JTextField();
		tvN.setColumns(10);
		
		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(index >= 0 ){
					deleteSheet(index);
				}
			}

			
		});
		
		JButton btnOpen = new JButton("Open");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(index>=0){
					Sheet sheet = book.getSheets().get(index);
					SheetWindow.newInstance(sheet).setVisible(true);
					
				}
			}
		});
		
		btnSaveToCsv = new JButton("Save to csv");
		btnSaveToCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(Sheet s: book.getSheets()){
					//saveToCSV
				}
				
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(list, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblName)
						.addComponent(lblM)
						.addComponent(lblN))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnOpen, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
						.addComponent(tvName, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(tvN, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
							.addComponent(tvM, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE))
						.addComponent(btnAdd, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
						.addComponent(btnDelete, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
						.addComponent(btnSaveToCsv, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(list, GroupLayout.PREFERRED_SIZE, 357, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblName)
								.addComponent(tvName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblM)
								.addComponent(tvM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblN)
								.addComponent(tvN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnAdd)
							.addGap(18)
							.addComponent(btnDelete)
							.addGap(18)
							.addComponent(btnOpen)
							.addGap(27)
							.addComponent(btnSaveToCsv)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	private void refreshViews() {
		model.removeAllElements();
		for(Sheet s: book.getSheets()){
			model.addElement(s);
		}
	}
	
	private void addSheet(Sheet s){
		model.addElement(s);
		book.addSheet(s);
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	private void deleteSheet(int index) {
		model.remove(index);
		book.removeSheetByPosition(index);
	}
	
	public static BookWindow newInstance(Book b){
		BookWindow bw = new BookWindow();
		bw.setBook(b);
		bw.refreshViews();
		return bw;
	}
}
