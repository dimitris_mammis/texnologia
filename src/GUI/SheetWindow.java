package GUI;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import models.And;
import models.BoolCell;
import models.Cell;
import models.Concat;
import models.FuncCell;
import models.Log;
import models.Log10;
import models.Max;
import models.Mean;
import models.Median;
import models.Min;
import models.Mult;
import models.Not;
import models.Or;
import models.RealCell;
import models.Remove;
import models.Sheet;
import models.StringCell;
import models.Sum;
import models.Trim;

public class SheetWindow extends JFrame {

	private JPanel contentPane;
	
	private Sheet sheet;
	private JTable table;
	private JTextField tvCellValue;

	private JComboBox spinnerSimple;
	private JComboBox spinnerFunc;
	private JLabel lblSelectFunction;
	private JLabel lblStart;
	private JLabel lblStartY;
	private JTextField tfStartX;
	private JTextField tfStartY;
	private JLabel lblStopX;
	private JLabel lblStopY;
	private JTextField ftStopX;
	private JTextField tfStopY;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JTextField tfPlotTilte;
	private JTextField tfPlotX;
	private JTextField tfPlotY;
	private JTextField tfPlotStartX;
	private JTextField tfStopPlotX;
	private JLabel lblStartX_1;
	private JLabel lblStopY_1;
	private JTextField tfPlotStartY;
	private JTextField tfPlotStopY;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SheetWindow frame = new SheetWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SheetWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 829, 687);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		table = new JTable();
		
		table.setModel(new DefaultTableModel(
				new Object[][] {
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
					{null, null, null, null, null, null, null, null, null, null, null, null},
				},
				new String[] {
						"New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column"
					}
			));
		
		JLabel lblRealboolstring = new JLabel("Select cell type");
		
		tvCellValue = new JTextField();
		tvCellValue.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addSimpleCell();
			}

			
		});
		
		JLabel lblValue = new JLabel("Value (for bool 1/0)");
		
		spinnerSimple = new JComboBox();
		spinnerSimple.addItem(new String("Real"));
		spinnerSimple.addItem(new String("String"));
		spinnerSimple.addItem(new String("Bool"));
		
		spinnerFunc = new JComboBox();
		
		spinnerFunc.addItem(new String("log10"));
		spinnerFunc.addItem(new String("log"));
		spinnerFunc.addItem(new String("sum"));
		spinnerFunc.addItem(new String("mult"));
		spinnerFunc.addItem(new String("trim"));
		spinnerFunc.addItem(new String("concat"));
		spinnerFunc.addItem(new String("remove"));
		spinnerFunc.addItem(new String("min"));
		spinnerFunc.addItem(new String("max"));
		spinnerFunc.addItem(new String("mean"));
		spinnerFunc.addItem(new String("median"));
		spinnerFunc.addItem(new String("and"));
		spinnerFunc.addItem(new String("or"));
		spinnerFunc.addItem(new String("not"));
		
		lblSelectFunction = new JLabel("Select Function");
		
		lblStart = new JLabel("Start X");
		
		lblStartY = new JLabel("Start Y");
		
		tfStartX = new JTextField();
		tfStartX.setColumns(10);
		
		tfStartY = new JTextField();
		tfStartY.setColumns(10);
		
		lblStopX = new JLabel("Stop X");
		
		lblStopY = new JLabel("Stop Y");
		
		ftStopX = new JTextField();
		ftStopX.setColumns(10);
		
		tfStopY = new JTextField();
		tfStopY.setColumns(10);
		
		btnNewButton = new JButton("Add Functon");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addFunctionCell();
			}

			
		});
		
		JButton btnBarplot = new JButton("Barplot");
		btnBarplot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				barplot();
			}

			
		});
		
		btnNewButton_1 = new JButton("Line plot");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				linePlot();
			}

			
		});
		
		JLabel lblPlot = new JLabel("Plot");
		
		JLabel lblTitle = new JLabel("Title");
		
		tfPlotTilte = new JTextField();
		tfPlotTilte.setColumns(10);
		
		JLabel lblXaxis = new JLabel("X-axis");
		
		tfPlotX = new JTextField();
		tfPlotX.setColumns(10);
		
		JLabel lblYaxis = new JLabel("Y-axis");
		
		tfPlotY = new JTextField();
		tfPlotY.setColumns(10);
		
		JLabel lblStartX = new JLabel("Start X");
		
		JLabel lblStopX_1 = new JLabel("Stop X");
		
		tfPlotStartX = new JTextField();
		tfPlotStartX.setColumns(10);
		
		tfStopPlotX = new JTextField();
		tfStopPlotX.setColumns(10);
		
		lblStartX_1 = new JLabel("Start Y");
		
		lblStopY_1 = new JLabel("Stop Y");
		
		tfPlotStartY = new JTextField();
		tfPlotStartY.setColumns(10);
		
		tfPlotStopY = new JTextField();
		tfPlotStopY.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(27)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 492, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblRealboolstring)
						.addComponent(spinnerSimple, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnAdd, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
						.addComponent(tvCellValue, 144, 144, 144)
						.addComponent(lblValue)
						.addComponent(spinnerFunc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblSelectFunction)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(lblStart)
								.addGap(26)
								.addComponent(lblStartY))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
									.addGroup(gl_contentPane.createSequentialGroup()
										.addComponent(lblStopX)
										.addGap(30))
									.addGroup(gl_contentPane.createSequentialGroup()
										.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(ftStopX, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
											.addComponent(tfStartX, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
										.addGap(18)))
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(lblStopY)
									.addComponent(tfStartY, 0, 0, Short.MAX_VALUE)
									.addComponent(tfStopY, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))))
						.addComponent(btnBarplot, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblXaxis)
								.addComponent(lblTitle))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(tfPlotTilte, GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
								.addComponent(tfPlotX, 214, 214, 214)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblYaxis)
									.addPreferredGap(ComponentPlacement.UNRELATED))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(tfPlotStartX, 0, 0, Short.MAX_VALUE)
										.addComponent(lblStartX, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(10)))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(tfPlotY, 215, 215, 215)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(12)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(tfStopPlotX, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
										.addComponent(lblStopX_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(Alignment.LEADING, gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(tfPlotStopY, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
											.addComponent(lblStopY_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
						.addComponent(lblPlot)
						.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 267, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(tfPlotStartY, 0, 0, Short.MAX_VALUE)
							.addComponent(lblStartX_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblRealboolstring)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(spinnerSimple, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(19)
							.addComponent(lblValue)
							.addGap(7)
							.addComponent(tvCellValue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(btnAdd)
							.addGap(42)
							.addComponent(lblSelectFunction)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(spinnerFunc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblStart)
								.addComponent(lblStartY))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfStartX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfStartY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblStopX)
								.addComponent(lblStopY))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(ftStopX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfStopY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton)
							.addGap(25)
							.addComponent(lblPlot)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblTitle)
								.addComponent(tfPlotTilte, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblXaxis)
								.addComponent(tfPlotX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblYaxis)
								.addComponent(tfPlotY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblStartX)
								.addComponent(lblStopX_1))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfPlotStartX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfStopPlotX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblStartX_1)
								.addComponent(lblStopY_1))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(tfPlotStartY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfPlotStopY, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnBarplot))
						.addComponent(table, GroupLayout.DEFAULT_SIZE, 638, Short.MAX_VALUE))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	public void refreshTable(){
		
		for(int i=0;i<sheet.getM();i++){
			for(int j=0;j<sheet.getN();j++){
				Cell cell = sheet.getCells()[i][j];
				if(cell instanceof FuncCell){
					FuncCell c = (FuncCell)cell;
					c.calculateValue();
				}
			}
		}

		
		String str[] = new String [sheet.getN()];
		
		for(int i=0;i<sheet.getN();i++){
			str[i] = ""+i;
		}
		
		
		DefaultTableModel model = new DefaultTableModel(sheet.getCells(),str); 
		
		table.setModel(model);
	}

	public void setSheet(Sheet sheet) {
		this.sheet = sheet;
	}
	
	private void addSimpleCell() {
		int row = table.getSelectedRow();
		int column = table.getSelectedColumn();
		int index = spinnerSimple.getSelectedIndex();
		String value = tvCellValue.getText().toString();
		if(index == 0){
			Cell c = new RealCell(row, column, Double.parseDouble(value));
			addCell(c);
		}else if(index == 1){
			Cell c = new StringCell(row, column, value);
			addCell(c);
		}else if(index == 2){
			Cell c = new BoolCell(row, column, value.equals("1"));
			addCell(c);
		}
		
	}
	
	private void addCell(Cell c) {
		sheet.addCell(c, c.getX(), c.getY());
		refreshTable();
	}
	
	private void addFunctionCell() {
		int startX = Integer.parseInt(tfStartX.getText());
		int startY = Integer.parseInt(tfStartY.getText());
		int stopX = Integer.parseInt(ftStopX.getText());
		int stopY = Integer.parseInt(tfStopY.getText());
		
		Cell[][] cells = sheet.getCells();
		
		ArrayList<Cell> l = new ArrayList<Cell>();
		
		for(int i=startX;i<=stopX;i++){
			for(int j=startY;j<=stopY;j++){
				l.add(cells[i][j]);
			}
		}
		
		int row = table.getSelectedRow();
		int col = table.getSelectedColumn();
		
		
		String selectedItem = (String)spinnerFunc.getSelectedItem();
		FuncCell f = null;
		
		if(selectedItem.equals("or")){
			f = new Or(row, col, l);
		}else if(selectedItem.equals("not")){
			f = new Not(row, col, l);
		}else if(selectedItem.equals("and")){
			f = new And(row, col, l);
		}else if(selectedItem.equals("max")){
			f = new Max(row, col, l);
		}else if(selectedItem.equals("mean")){
			f = new Mean(row, col, l);
		}else if(selectedItem.equals("min")){
			f = new Min(row, col, l);
		}else if(selectedItem.equals("median")){
			f = new Median(row, col, l);
		}else if(selectedItem.equals("log10")){
			f = new Log10(row, col, l);
		}else if(selectedItem.equals("sum")){
			f = new Sum(row, col, l);
		}else if(selectedItem.equals("mult")){
			f = new Mult(row, col, l);
		}else if(selectedItem.equals("log")){
			f = new Log(row, col, l);
		}else if(selectedItem.equals("trim")){
			f = new Trim(row, col, l);
		}else if(selectedItem.equals("remove")){
			f = new Remove(row, col, l);
		}else if(selectedItem.equals("concat")){
			f = new Concat(row, col, l);
		}
		f.calculateValue();
		cells[row][col] = f;
		refreshTable();
	}
	
	public String getTitle(){
		return tfPlotTilte.getText();
	}
	
	public String getXAxis(){
		return tfPlotX.getText();
	}
	
	public String getYAxis(){
		return tfPlotY.getText();
	}
	
	public DefaultCategoryDataset getData(){
		int startX = Integer.parseInt(tfPlotStartX.getText());
		int startY = Integer.parseInt(tfPlotStartY.getText());
		int stopX = Integer.parseInt(tfStopPlotX.getText());
		int stopY = Integer.parseInt(tfPlotStopY.getText());
		
		DefaultCategoryDataset objDataset = new DefaultCategoryDataset();
		
		int count = 0;
		for(int i=startX;i<=stopX;i++){
			for(int j=startY; j<=stopY;j++){
				try{
					Double value = (Double)sheet.getCells()[i][j].getValue();
					objDataset.setValue(value,"Q1",""+count);
					count++;
				}catch(Exception e){
					
				}
				
			}
		}
		
		return objDataset;
	}
	
	private void linePlot() {
		JFreeChart objChart = ChartFactory.createLineChart(
			       getTitle(),     //Chart title
			    getXAxis(),     //Domain axis label
			    getYAxis(),         //Range axis label
			    getData(),         //Chart Data 
			    PlotOrientation.VERTICAL, // orientation
			    false,             // include legend?
			    true,             // include tooltips?
			    false             // include URLs?
			);
		
		ChartFrame frame = new ChartFrame("Chart", objChart);
		frame.pack();
		frame.setVisible(true);
		
	}
	
	private void barplot() {
		
		
		JFreeChart objChart = ChartFactory.createBarChart(
			       getTitle(),     //Chart title
			    getXAxis(),     //Domain axis label
			    getYAxis(),         //Range axis label
			    getData(),         //Chart Data 
			    PlotOrientation.VERTICAL, // orientation
			    false,             // include legend?
			    true,             // include tooltips?
			    false             // include URLs?
			);
		
		ChartFrame frame = new ChartFrame("Chart", objChart);
		frame.pack();
		frame.setVisible(true);
		
	}

	public static SheetWindow newInstance(Sheet s){
		SheetWindow sw = new SheetWindow();
		sw.setSheet(s);
		sw.refreshTable();
		return sw;
	}
}
